package cr.ac.ucenfotec.bl;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Objects;

public class BL {
    private static ArrayList<Cliente> clientes = new ArrayList<>();
    private static ArrayList<Cuenta> cuentas = new ArrayList<>();


    ///CLIENTE

    public String ingresarCliente(String nombre, int identificacion, int edad, int dia, int mes, int year, String direccion) {

        //Existe cliente
        if (existeCliente(identificacion)) {
            return "El cliente ya existe";
        }
        //Inicializa el objeto cr.ac.ucenfotec.bi.Cliente
        Cliente tmpCliente = new Cliente(nombre, identificacion, edad, LocalDate.of(year, mes, dia), direccion);
        clientes.add(tmpCliente); // Añade el cliente al array list.
        return "cr.ac.ucenfotec.bi.Cliente registrada con exito";
    }

    public ArrayList<Cliente> listarCliente() {
        return clientes;
    }

    public static boolean existeCliente(Integer identificacion) {
        for (int i = 0; i < clientes.size(); i++) {
            Cliente c = clientes.get(i);
            if (Objects.equals(c.getIdentificacion(), identificacion)) {
                return true;
            }
        }
        return false;
    }

    private Cliente buscarCliente(int identificacionCliente) {
        for (Cliente cliente : clientes) {
            if (Objects.equals(cliente.getIdentificacion(), identificacionCliente)) {
                return cliente;
            }
        }
        return null;
    }


    ///CUENTA

/*
    public String crearCuenta(int numeroCuenta, int saldo, int identificacionCliente) {

        Cliente cliente = buscarCliente(identificacionCliente);
        if (cliente != null) {
            Cuenta tmpCuenta = new Cuenta (numeroCuenta,saldo,fechaCreacion);
            cuentas.add(tmpCuenta);
            cliente.agregarCuenta(tmpCuenta);
            return "El curso fue agregado de manera correcta!";


        } else
            return "Cliente no existe en el sistema";
    }
    */


    public String registrarCorriente(int numeroCuenta, double saldo,int dia, int mes, int year, int identificacionCliente) {

        Cliente cliente = buscarCliente(identificacionCliente);
        if (cliente != null) {
            Corriente tmpCorriente = new Corriente (numeroCuenta,saldo,LocalDate.of(year, mes, dia));
            cuentas.add(tmpCorriente);
            cliente.agregarCuenta(tmpCorriente);
            return "La Cuenta Corriente fue creada de manera correcta!";


        } else
            return "Cliente no existe en el sistema";
    }

    public String registrarAhorro(int numeroCuenta, double saldo,int dia, int mes, int year,double interes, int identificacionCliente) {

        Cliente cliente = buscarCliente(identificacionCliente);
        if (cliente != null) {
            Ahorro tmpAhorro = new Ahorro (numeroCuenta,saldo,LocalDate.of(year, mes, dia),interes);
            cuentas.add(tmpAhorro);
            cliente.agregarCuenta(tmpAhorro);
            return "La Cuenta Ahorro fue creada de manera correcta!";


        } else
            return "Cliente no existe en el sistema";
    }



    public ArrayList<String> listarCorriente(){
        ArrayList<String>lista= new ArrayList<>();
        for(Cuenta cuenta: cuentas){
            if(cuenta instanceof Corriente){
                lista.add(cuenta.toString());
            }
        }
        return lista;
    }

    public ArrayList<String> listarAhorro(){
        ArrayList<String>lista= new ArrayList<>();
        for(Cuenta cuenta: cuentas){
            if(cuenta instanceof Ahorro){
                lista.add(cuenta.toString());
            }
        }
        return lista;
    }









/*
    public  ArrayList<Cuenta> listarCuenta(){
        return cuentas;
    }*/

    public  boolean existeCuenta(Integer numeroCuenta){
        for(int i=0; i<cuentas.size(); i++){
            Cuenta c = cuentas.get(i);
            if(c.getNumeroCuenta()==(numeroCuenta)){
                return true;
            }
        }
        return false;
    }




    public  boolean deposito(Integer monto, Integer saldo){
        boolean montoCorrecto=true;
        if(monto<=0){
            montoCorrecto= false;
        }else{
            saldo=saldo+monto;
        }
        return montoCorrecto;

    }


    private Cuenta buscarCuenta(int busNumeroCuenta) {
        for (Cuenta cuenta : cuentas) {
            if (Objects.equals(cuenta.getNumeroCuenta(), busNumeroCuenta)) {
                return cuenta;
            }
        }
        return null;
    }

    public String realizarDeposito(int identificacionCliente, int busNumeroCuenta, int monto){
        Cliente cliente = buscarCliente(identificacionCliente);
        if(cliente!=null){
            Cuenta cuenta=buscarCuenta(busNumeroCuenta);
            if (cuenta!=null){
                cuenta.ingresarDinero(monto);
                return "";
            }else
                return "Cuenta no existe en sistema";
        }else
            return "Cliente no existe en sistema";
    }

    public String realizarRetiro(int identificacionCliente, int busNumeroCuenta, int monto){
        Cliente cliente = buscarCliente(identificacionCliente);
        if(cliente!=null){
            Cuenta cuenta=buscarCuenta(busNumeroCuenta);
            if (cuenta!=null){
                cuenta.retirarDinero(monto);
                return "";
            }else
                return "Cuenta no existe en sistema";
        }else
            return "Cliente no existe en sistema";
    }



}



