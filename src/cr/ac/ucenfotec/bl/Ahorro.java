package cr.ac.ucenfotec.bl;

import java.time.LocalDate;

public class Ahorro extends Cuenta {
    private double interes;

    public Ahorro() {
        super();
    }

    public Ahorro(int numeroCuenta, double saldo, LocalDate fechaCreacion, double interes) {
        super(numeroCuenta, saldo,fechaCreacion);
        this.interes = interes;
    }

    //Getters

    public double getInteres() {
        return interes;
    }


    //Setters

    public void setInteres(double interes) {
        this.interes = interes;
    }

    //Metodos
    public double calcularInteres(){
        return getSaldo()*interes;
    }

    public  void aplicarInteres(){
        double intereses= calcularInteres();

    }

    @Override
    public void ingresarDinero(double deposito) {
        if(deposito>0){
            setSaldo(getSaldo()+deposito);
            System.out.println("Deposito realizado con exito");
        }else{
            System.out.println("Un monto negativo no puede ser depositado");
        }

    }

    @Override
    public void retirarDinero(double cantidad) {
        if (cantidad>0){
            if(cantidad<=getSaldo()*0.50 && getSaldo()>100000){
                setSaldo(getSaldo()-cantidad);
                System.out.println("Retiro realizado con exito");
            }
        }else{
            System.out.println("Un saldo negativo no puede ser retirado");
        }

    }


    //To String


}