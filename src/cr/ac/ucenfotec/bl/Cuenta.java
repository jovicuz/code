package cr.ac.ucenfotec.bl;

import java.time.LocalDate;

public  abstract class Cuenta {
    private int numeroCuenta;
    private double saldo;
    private LocalDate fechaCreacion;
    private int dia;
    private int mes;
    private int year;
    private int antiguedad;




    public Cuenta() {

    }

    public Cuenta(int numeroCuenta,double saldo, LocalDate fechaCreacion) {
        this.numeroCuenta = numeroCuenta;
        this.saldo = saldo;
        this.fechaCreacion=fechaCreacion;
        calcularAntiguedad();
    }


    //Setters

    public void setNumeroCuenta(int numeroCuenta) {
        this.numeroCuenta = numeroCuenta;
    }

    public void setSaldo(double saldo) {
        this.saldo = saldo;
    }

    public void setFechaCreacion(LocalDate fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }


    //Getters

    public int getNumeroCuenta() {
        return numeroCuenta;
    }

    public double getSaldo() {
        return saldo;
    }

    public LocalDate getFechaCreacion() {
        return fechaCreacion;
    }

    //Metodos

    public abstract void ingresarDinero(double deposito);

    public  abstract void retirarDinero(double cantidad);

    public void calcularAntiguedad() {
        this.antiguedad = LocalDate.now().getYear() - fechaCreacion.getYear();
    }


    //To string

    @Override
    public String toString() {
        return "Cuenta{" +
                "saldo=" + saldo +
                ", numero de cuenta=" + numeroCuenta +
                '}';
    }
}