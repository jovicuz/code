package cr.ac.ucenfotec.bl;

import java.time.LocalDate;
import java.util.ArrayList;

public class Cliente {
    private String nombre;
    private int identificacion;
    private LocalDate fechaNacimiento;
    private int  edad;
    private int dia;
    private int mes;
    private int year;
    private String direccion;
    private ArrayList<Cuenta> cuentas;  //Relación agregación Multiple


    public Cliente() {

    }

    public Cliente(String nombre, int identificacion, int edad, LocalDate fechaNacimiento, String direccion) {
        this.nombre = nombre;
        this.identificacion = identificacion;
        this.edad = edad;
        this.fechaNacimiento = fechaNacimiento;
        this.direccion = direccion;
        this.cuentas = new ArrayList<>();
    }

    //Setters

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setIdentificacion(int identificacion) {
        this.identificacion = identificacion;
    }

    public void setFechaNacimiento(LocalDate fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public void setDia(int dia) {
        this.dia = dia;
    }

    public void setMes(int mes) {
        this.mes = mes;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public void setCuentas(ArrayList<Cuenta> cuentas) {
        this.cuentas = cuentas;
    }


    //Getters


    public String getNombre() {
        return nombre;
    }

    public int getIdentificacion() {
        return identificacion;
    }

    public LocalDate getFechaNacimiento() {
        return fechaNacimiento;
    }

    public int getEdad() {
        return edad;
    }

    public int getDia() {
        return dia;
    }

    public int getMes() {
        return mes;
    }

    public int getYear() {
        return year;
    }

    public String getDireccion() {
        return direccion;
    }

    public ArrayList<Cuenta> getCuentas() {
        return this.cuentas;
    }

    public void agregarCuenta(Cuenta cuenta){
        cuentas.add(cuenta);


    }


    //To String
    @Override
    public String toString() {
        return "cr.ac.ucenfotec.bi.Cliente{" +
                "Nombre del cliente= '" + nombre + '\'' +
                ",Numero de identificacion= '" + identificacion + '\'' +
                ", Fecha de nacimiento= " + fechaNacimiento +
                ", Edad= " + edad +
                ", Dirección= " + direccion +
                ", Cuenta= " + cuentas.toString() +

                '}';
    }



}
