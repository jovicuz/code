package cr.ac.ucenfotec.bl;

import java.time.LocalDate;

public class Corriente extends Cuenta {

    public Corriente() {
        super();
    }

    public Corriente(int numeroCuenta, double saldo, LocalDate fechaCreacion) {
        super(numeroCuenta, saldo, fechaCreacion);
    }

    @Override
    public void ingresarDinero(double deposito) {
        if(deposito>0){
            setSaldo(getSaldo()+deposito);
            System.out.println("Deposito realizado con exito");
        }else{
            System.out.println("Un monto negativo no puede ser depositado");
        }

    }

    @Override
    public void retirarDinero(double cantidad) {
        if (cantidad>0){
            if(cantidad<=getSaldo()){
                setSaldo(getSaldo()-cantidad);
                System.out.println("Retiro realizado con exito");
            }
        }else{
            System.out.println("Un saldo negativo no puede ser retirado");
        }

    }
}
