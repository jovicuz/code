package cr.ac.ucenfotec.ui;

import cr.ac.ucenfotec.bl.BL;
import cr.ac.ucenfotec.bl.Cliente;
import cr.ac.ucenfotec.bl.Cuenta;


import java.io.*;
import java.util.Random;
import java.util.ArrayList;

public class UI {

    static BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
    static PrintStream out = System.out;
    static private BL gestor= new BL();


    public static void main(String[] args) throws IOException {
        // write your code here
        int opcion = -1;
        inicializarPrograma();//esto es opcional
        do {
            mostrarMenu();
            opcion = seleccionarOpcion();
            procesarOpcion(opcion);
        } while (opcion != 0);
    }

    static void mostrarMenu() {
        //TODO: llenar las opciones de menú según el programa a desarrollar.
        out.println("1.Registrar Clientes");
        out.println("2.Listar Clientes");
        out.println("3.Crear Cuenta Corriente");
        out.println("4.Crear Cuenta Ahorro");
        out.println("5.Crear Cuenta Ahorro Programado");
        out.println("6.Realizar depósito");
        out.println("7.Realizar retiro");
        out.println("8.Mostrar saldo de cuenta.1");
        out.println("0.Salir");
    }




    static void procesarOpcion(int pOpcion) throws IOException {
        switch (pOpcion) {
            case 1:
                ingresarCliente();
                break;
            case 2:
                listarCliente();
                break;
            case 3:
                crearCuenta();
                break;
            case 4:
                crearCuentaAhorro();
                break;
            case 5:
                crearCuentaAhorroProgramado();
                break;
            case 6:
                realizarDeposito();
                break;
            case 7:
                realizarRetiro();
                break;
            case 8:
                mostrarSaldo();
                break;
            case 9:
                listarCorrientes();
                break;
            case 0:
                out.println("Gracias por usar el programa");
                break;
            default:
                out.println("Opción inválida");
                break;
        }
    }

    static void inicializarPrograma() {

    }

    static int seleccionarOpcion() throws IOException {
        out.println("Digite la opcion");
        return Integer.parseInt(in.readLine());
    }

    static void ingresarCliente() throws IOException{
        out.println("Digite el nombre");
        String nombre = in.readLine();
        out.println("Digite el número de identificación");
        int identificacion= Integer.parseInt(in.readLine());
        out.println("Digite la edad");
        int edad = Integer.parseInt(in.readLine());
        out.println("Digite el día de nacimiento");
        int dia = Integer.parseInt(in.readLine());
        out.println("Digite el mes de nacimiento");
        int mes = Integer.parseInt(in.readLine());
        out.println("Digite el año de nacimiento");
        int year = Integer.parseInt(in.readLine());
        out.println("Digite la dirección");
        String direccion= in.readLine();
        String mensaje= gestor.ingresarCliente(nombre, identificacion, edad, dia,mes,year, direccion);
         out.println(mensaje);
    }


    static void listarCliente(){
        ArrayList<Cliente> lista= gestor.listarCliente();
        for(int i=0;i<lista.size();i++){
            Cliente c = new Cliente();
            c=lista.get(i);
            out.println(c.toString());

        }
    }


        static void crearCuenta() throws IOException{
            out.println("Bienvenido");
            out.println("Digíte su número de identificación para crear una cuenta");
            int identificacionCliente=Integer.parseInt(in.readLine());
            Random rnd= new Random();
            int number= rnd.nextInt(9999999);
            int numeroCuenta=number;
            double saldo;
            out.println("Digite el día de creación");
            int dia = Integer.parseInt(in.readLine());
            out.println("Digite el mes de creación");
            int mes = Integer.parseInt(in.readLine());
            out.println("Digite el año de creación");
            int year = Integer.parseInt(in.readLine());
            do{
                out.println("Haga un deposito mayor o igual a ₡50000");
                saldo= Integer.parseInt(in.readLine());
            }while(!(saldo>=50000));
            String mensaje= gestor.registrarCorriente(numeroCuenta,saldo,dia,mes,year, identificacionCliente);
            out.println(mensaje);

        }


        static void crearCuentaAhorro() throws IOException{
            out.println("Bienvenido");
            out.println("Digíte su número de identificación para crear una cuenta");
            int identificacionCliente=Integer.parseInt(in.readLine());
            Random rnd= new Random();
            int number= rnd.nextInt(9999999);
            int numeroCuenta=number;
            double saldo;
            double interes=0;
            out.println("Digite el día de creación");
            int dia = Integer.parseInt(in.readLine());
            out.println("Digite el mes de creación");
            int mes = Integer.parseInt(in.readLine());
            out.println("Digite el año de creación");
            int year = Integer.parseInt(in.readLine());
            do{
                out.println("Haga un deposito mayor o igual a ₡50000");
                saldo= Integer.parseInt(in.readLine());
            }while(!(saldo>=50000));
            String mensaje= gestor.registrarAhorro(numeroCuenta,saldo,dia,mes,year,interes, identificacionCliente);
            out.println(mensaje);

        }

    static void crearCuentaAhorroProgramado()throws IOException{

    }


    static void realizarDeposito()throws IOException{
        int identificacionCliente;
        int busNumeroCuenta;
        int monto;
        out.println("Digite su identificación");
        identificacionCliente = Integer.parseInt(in.readLine());
        out.println("Las cuentas disponibles son las siguientes");
        out.println("*******Cuentas Corrientes****");
        listarCorrientes();
        out.println("*******Cuentas Ahorro****");
        listarAhorros();
        out.println("Digite el numero de cuenta");
        busNumeroCuenta = Integer.parseInt(in.readLine());
        out.println("Digite el monto que desea depositar");
        monto = Integer.parseInt(in.readLine());
        String mensaje= gestor.realizarDeposito(identificacionCliente,busNumeroCuenta,monto);
        out.println(mensaje);






    }

    static void realizarRetiro()throws IOException{
        int identificacionCliente;
        int busNumeroCuenta;
        int monto;
        out.println("Digite su identificación");
        identificacionCliente = Integer.parseInt(in.readLine());
        out.println("Las cuentas disponibles son las siguientes");
        out.println("*******Cuentas Corrientes****");
        listarCorrientes();
        out.println("*******Cuentas Ahorro****");
        listarAhorros();
        out.println("Digite el numero de cuenta");
        busNumeroCuenta = Integer.parseInt(in.readLine());
        out.println("Digite el monto que desea depositar");
        monto = Integer.parseInt(in.readLine());
        String mensaje= gestor.realizarRetiro(identificacionCliente,busNumeroCuenta,monto);
        out.println(mensaje);




    }

    static void mostrarSaldo(){

    }

    /*
    static void listarCuenta(){
        ArrayList<Cuenta> listaCuenta= gestor.listarCorriente();
        for(int i=0;i<listaCuenta.size();i++){
            Cuenta cu = new Corriente();
            cu=listaCuenta.get(i);
            out.println(cu.toString());
        }

    }*/

    public static void listarCorrientes(){
        System.out.println("*****Listado de cuentas Corrientes inscritas *****");
        String[] datos = gestor.listarCorriente().toArray(new String[0]);
        for (int i =0;i < datos.length ;i++) {
            System.out.println(datos[i]);
        }
    }


    public static void listarAhorros(){
        System.out.println("*****Listado de cuentas de ahorro inscritas *****");
        String[] datos = gestor.listarAhorro().toArray(new String[0]);
        for (int i =0;i < datos.length ;i++) {
            System.out.println(datos[i]);
        }
    }






}


